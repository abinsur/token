# Token

For HTTP/HTTPS operations use plain username and password. 

Personal Access Token credential, e.g. `url = https://{tokenname}:{token}@codeberg.org/{username}/{repo}.git`